# Models

### mobilebert_tiny_truncated_pretrained
Original MobileBERT-tiny model, but truncated to 21 layers. It is only pretrained, not finetuned for a specific task.

### mobilebert_tiny_truncated_sst2_fp32
Finetuned for SST2 using FP32

### mobilebert_tiny_truncated_sst2_qat_approx
Finetuned for SST2 using Posit8 quantization-aware training, including Posit approximations for exponential and reciprocal

### mobilebert_tiny_truncated_sst2_qat_approx
Finetuned for SST2 using Posit7 quantization-aware training, including Posit approximations for exponential and reciprocal
