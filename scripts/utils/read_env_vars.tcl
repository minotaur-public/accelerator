# Read and set relevant environment variables
set ENV_VARS {BLOCK CLOCK_PERIOD DATATYPE IC_DIMENSION OC_DIMENSION INPUT_BUFFER_SIZE WEIGHT_BUFFER_SIZE ACCUM_BUFFER_SIZE TECHNOLOGY CATAPULT_BUILD_DIR}

foreach var $ENV_VARS {
  set $var [exec echo $::env($var)]
}

set ROOT [file normalize $::env(PROJ_ROOT)]
