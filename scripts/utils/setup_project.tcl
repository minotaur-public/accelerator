# Create project folder
set project_folder "$ROOT/$CATAPULT_BUILD_DIR/$BLOCK"
if { [file exists $project_folder] } {
  file delete -force -- $project_folder
}

project new -dir $project_folder
project save

# Set log file
logfile move "$ROOT/$CATAPULT_BUILD_DIR/$BLOCK.log"

# Configure options
solution options set Project/SolutionName $BLOCK
solution options set Message/ErrorOverride ASSERT-1 -remove
solution options set Input/TargetPlatform x86_64
solution options set Input/CppStandard c++17
solution options set Input/CompilerFlags "-D$DATATYPE -DIC_DIMENSION=$IC_DIMENSION -DOC_DIMENSION=$OC_DIMENSION -DINPUT_BUFFER_SIZE=$INPUT_BUFFER_SIZE -DWEIGHT_BUFFER_SIZE=$WEIGHT_BUFFER_SIZE -DACCUM_BUFFER_SIZE=$ACCUM_BUFFER_SIZE"
solution options set Input/SearchPath "$ROOT/lib"
solution options set Output/OutputVHDL false
solution options set Architectural/DefaultMemMapThreshold 256
solution options set Architectural/DefaultRegisterThreshold 4096
solution options set Flows/Enable-SCVerify yes
solution options set Flows/VCS/SYSC_VERSION 2.3.3
solution options set Flows/VCS/VLOGAN_OPTS {+v2k -timescale=1ns/10ps +notimingcheck +define+UNIT_DELAY}
# solution options set Flows/VCS/VCSSIM_OPTS {+fsdbfile+dump.fsdb +fsdb+all=on +fsdb+dumpon+0}
solution options set Flows/VCS/VCS_DOFILE dump.do
solution options set Flows/VCS/COMP_FLAGS "-O3 -Wall -Wno-unknown-pragmas -I$ROOT/lib/ -I$ROOT/lib/xtensor/include -I$ROOT/lib/xtl/include -I$ROOT/src/ -I$ROOT/ -I$::env(CONDA_PREFIX)/include -DSIM_$BLOCK -D$DATATYPE -DIC_DIMENSION=$IC_DIMENSION -DOC_DIMENSION=$OC_DIMENSION -DINPUT_BUFFER_SIZE=$INPUT_BUFFER_SIZE -DWEIGHT_BUFFER_SIZE=$WEIGHT_BUFFER_SIZE -DACCUM_BUFFER_SIZE=$ACCUM_BUFFER_SIZE -std=c++17"
solution options set Flows/VCS/VCSELAB_OPTS "-timescale=1ns/1ps -sysc=blocksync -lstdc++fs -L$::env(CONDA_PREFIX)/lib -LDFLAGS \"-Wl,--enable-new-dtags -Wl,-R,$::env(CONDA_PREFIX)/lib\" -labsl_log_internal_message -labsl_log_internal_check_op -lprotobuf"

flow package require /SCVerify
flow package option set /SCVerify/USE_VCS true

go new

# Add source files
solution file add $ROOT/src/Accelerator.h -type CHEADER

# Add testbench files
solution file add $ROOT/test/common/TestRunner.cc -type C++ -exclude true
solution file add $ROOT/test/common/Harness.cc -type C++ -exclude true
solution file add $ROOT/test/common/Utils.cc -type C++ -exclude true
solution file add $ROOT/test/common/GoldModel.cc -type C++ -exclude true
solution file add $ROOT/test/common/Simulation.cc -type C++ -exclude true
solution file add $ROOT/test/common/ArrayMemory.cc -type C++ -exclude true
solution file add $ROOT/test/common/DataLoader.cc -type C++ -exclude true
solution file add $ROOT/test/toolchain/MapOperation.cc -type C++ -exclude true

# Add network files
solution file add $ROOT/test/common/Network.cc -type C++ -exclude true
solution file add $ROOT/test/compiler/proto/param.pb.cc -type C++ -exclude true
